// console.log('Welcome to PizzaLand 🍕 !');
// console.log('Welcome to ', {title:'PizzaLand', emoji: '🍕'});
// console.warn()
// console.error()
// console.clear()
// let what = 'door';
// console.log('Hold', 'the', what );
// var name  = "";
// var url;
// const data = ['Regina', 'Napolitaine', 'Spicy'];
var html = "";

let data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];


/*for(let i = 0; i<data.length;i++){
    name = data[i];
    url = `images/${name.toLowerCase()}.jpg`;
    html = html +
        `<article class="pizzaThumbnail">
        <a href="${url}">
        <img src="${url}"/>
        <section>${name}</section>
        </a>
        </article> 
    `;
    document.querySelector('.pageContent').innerHTML = html
}*/

/*html = data.map( x => {
    url = `images/${x.name.toLowerCase()}.jpg`;
    return `<article class="pizzaThumbnail">
        <a href="${x.url}">
        <img src="${x.url}"/>
        <section>${x.name}</section>
        </a>
        </article>`;
}).join("");*/

/*data.forEach(element => {
    name = element;
    url = `images/${name.toLowerCase()}.jpg`;
    html = html +
        `<article class="pizzaThumbnail">
        <a href="${url}">
        <img src="${url}"/>
        <section>${name}</section>
        </a>
        </article> 
    `;
    document.querySelector('.pageContent').innerHTML = html
});*/

function compareNom(a, b) {
    if(a.name < b.name) return -1;
    else if(a.name > b.name) return 1;
    return 0;
}

function comparePetitPrixCroissant(a, b) {
    if(a.price_small < b.price_small) return -1;
    else if(a.price_small > b.price_small) return 1;
    return 0;
}

function compareGrandPrixCroissant(a, b) {
    if(a.price_large < b.price_large) return -1;
    else if(a.price_large > b.price_large) return 1;
    return 0;
}

function comparePetitPrixCroissantEtGrandPrixCroissant(a, b) {
    if(a.price_small < b.price_small) return -1;
    else if(a.price_small > b.price_small) return 1;
    else return compareGrandPrixCroissant(a,b);
}
  
data.sort(comparePetitPrixCroissantEtGrandPrixCroissant);

function baseFilter({base}=a){
    return base=='tomate';   
}

function prixFilter({price_small}=a){
    return price_small < 6;   
}

function occursFilter({name}=a){
    var nbLetter=0;
    const tab = name.split('');
    tab.forEach(letter => {
        if(letter == 'i') nbLetter++;  
    });
    console.log(nbLetter)
    return nbLetter==2;
}

data = data.filter(occursFilter);


data.forEach(({name, image, price_large, price_small}) => {
    html = html +`<article class="pizzaThumbnail">
        <a href="${image}">
            <img src="${image}" />
            <section>
                <h4>${name}</h4>
                <ul>
                    <li>Prix petit format : ${price_small} €</li>
                    <li>Prix grand format : ${price_large} €</li>
                </ul>
            </section>
        </a>
    </article>`;
});

document.querySelector('.pageContent').innerHTML = html
"use strict";

// console.log('Welcome to PizzaLand 🍕 !');
// console.log('Welcome to ', {title:'PizzaLand', emoji: '🍕'});
// console.warn()
// console.error()
// console.clear()
// let what = 'door';
// console.log('Hold', 'the', what );
// var name  = "";
// var url;
// const data = ['Regina', 'Napolitaine', 'Spicy'];
var html = "";
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
/*for(let i = 0; i<data.length;i++){
    name = data[i];
    url = `images/${name.toLowerCase()}.jpg`;
    html = html +
        `<article class="pizzaThumbnail">
        <a href="${url}">
        <img src="${url}"/>
        <section>${name}</section>
        </a>
        </article> 
    `;
    document.querySelector('.pageContent').innerHTML = html
}*/

/*html = data.map( x => {
    url = `images/${x.name.toLowerCase()}.jpg`;
    return `<article class="pizzaThumbnail">
        <a href="${x.url}">
        <img src="${x.url}"/>
        <section>${x.name}</section>
        </a>
        </article>`;
}).join("");*/

/*data.forEach(element => {
    name = element;
    url = `images/${name.toLowerCase()}.jpg`;
    html = html +
        `<article class="pizzaThumbnail">
        <a href="${url}">
        <img src="${url}"/>
        <section>${name}</section>
        </a>
        </article> 
    `;
    document.querySelector('.pageContent').innerHTML = html
});*/

function compareNom(a, b) {
  if (a.name < b.name) return -1;else if (a.name > b.name) return 1;
  return 0;
}

function comparePetitPrixCroissant(a, b) {
  if (a.price_small < b.price_small) return -1;else if (a.price_small > b.price_small) return 1;
  return 0;
}

function compareGrandPrixCroissant(a, b) {
  if (a.price_large < b.price_large) return -1;else if (a.price_large > b.price_large) return 1;
  return 0;
}

function comparePetitPrixCroissantEtGrandPrixCroissant(a, b) {
  if (a.price_small < b.price_small) return -1;else if (a.price_small > b.price_small) return 1;else return compareGrandPrixCroissant(a, b);
}

data.sort(comparePetitPrixCroissantEtGrandPrixCroissant);

function baseFilter() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : a,
      base = _ref.base;

  return base == 'tomate';
}

function prixFilter() {
  var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : a,
      price_small = _ref2.price_small;

  return price_small < 6;
}

function occursFilter() {
  var _ref3 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : a,
      name = _ref3.name;

  var nbLetter = 0;
  var tab = name.split('');
  tab.forEach(function (letter) {
    if (letter == 'i') nbLetter++;
  });
  console.log(nbLetter);
  return nbLetter == 2;
}

data = data.filter(occursFilter);
data.forEach(function (_ref4) {
  var name = _ref4.name,
      image = _ref4.image,
      price_large = _ref4.price_large,
      price_small = _ref4.price_small;
  html = html + "<article class=\"pizzaThumbnail\">\n        <a href=\"".concat(image, "\">\n            <img src=\"").concat(image, "\" />\n            <section>\n                <h4>").concat(name, "</h4>\n                <ul>\n                    <li>Prix petit format : ").concat(price_small, " \u20AC</li>\n                    <li>Prix grand format : ").concat(price_large, " \u20AC</li>\n                </ul>\n            </section>\n        </a>\n    </article>");
});
document.querySelector('.pageContent').innerHTML = html;
//# sourceMappingURL=main.js.map